from http.server import HTTPServer, BaseHTTPRequestHandler
import os


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.write_response(self.path.encode())

    def do_POST(self):
        content_length = int(self.headers.get('content-length', 0))
        body = self.rfile.read(content_length)

        self.write_response(body)

    def write_response(self, content):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(content)


def main(host, port):
    httpd = HTTPServer((host, port), SimpleHTTPRequestHandler)
    httpd.serve_forever()


if __name__ == "__main__":
    BIND_HOST = '0.0.0.0'

    main(BIND_HOST, int(os.environ['PORT']))
