FROM python:latest
RUN adduser --disabled-login deploy
USER deploy
WORKDIR /usr/src/app/
COPY . /usr/src/app/
CMD ["python", "server.py"]
